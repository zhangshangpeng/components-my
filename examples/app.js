import './app.css'
import Vue from 'vue'
import App from './views/App.vue'

new Vue({ // eslint-disable-line no-new
  el: '#app',
  render: h => h(App)
})
